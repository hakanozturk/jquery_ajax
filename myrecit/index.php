<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Insert</title>
</head>
<body>

    <input type="text" name="name" id="name">
    <input type="text" name="surname" id="surname">
    <input type="text" name="age" id="age">
    <button id="button">SEND</button>
    
    <script>
        $(document).ready(function(){
            $("#button").click(function(){
                var name=$("#name").val();
                var surname=$("#surname").val();
                var age=$("#age").val();
                $.ajax({
                    url:'sendmsg.php',
                    method:'POST',
                    data:{
                        name:name,
                        surname:surname,
                        age:age
                    },
                   success:function(data){
                       alert(data);
                   }
                });
            });
        });
    </script>
</body>

</html>


